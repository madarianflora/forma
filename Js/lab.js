// dropdown
const selectDropdown= document.querySelector(".dropdown-select"); //undefinnde 
const selected = document.querySelector(".selected");
const optionsList = document.querySelector(".dropdown-optionsList");
const dropdown  = document.querySelector(".dropdown-container");
const option = document.querySelectorAll(".option");

const verticalBar = document.querySelector(".navbar__bars");
const menu = document.querySelector(".navbar__menu");

if(selectDropdown) {selectDropdown.addEventListener("click", () => {   
    dropdown.classList.toggle("active");
})}
option.forEach(el => {   
     el.addEventListener("click", () => { 
        selected.innerHTML = el.innerHTML;      
 
        dropdown.classList.remove("active");    
    });
});

//Vertical menu Bar
verticalBar.addEventListener("click", ()=> {
  menu.classList.toggle("show");
});

window.addEventListener("mouseup", ()=> {
    menu.classList.remove("show");
})